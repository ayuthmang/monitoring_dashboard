//
// SAGE2 application: monitoring_dashboard
// by: Ayuth Mangmesap <ayuth.mang@gmail.com>
//
// Copyright (c) 2015
//

"use strict";

/* global  */


/* TODO: fetch csv from influxdb
// const uri = `http://35.197.145.223:8086/query?q=SELECT mean("usage_user") AS "mean_usage_user", mean("usage_system") AS "mean_usage_system" FROM "telegraf"."autogen"."cpu" WHERE time > now() - 15m AND time < now() AND "cpu"='cpu-total' GROUP BY time(2500ms) FILL(null)&format=csv`;

// fetch(uri, {
//     method: 'GET',
//     headers: {
//         'Accept': 'text/csv'
//     }
// }).then(res => res.text()).then(data => {
//     console.log(data)
// }).catch(err => {
//     console.error(err);
// })
*/

var monitoring_dashboard = SAGE2_App.extend({
	init: function(data) {
		// Create div into the DOM
		this.SAGE2Init("div", data);
		// Set the DOM id
		this.element.id = "div_" + data.id;
		// Set the background to black
		this.element.style.backgroundColor = 'white';

		// move and resize callbacks
		this.resizeEvents = "continuous"; // onfinish
		// this.moveEvents   = "continuous";

		// SAGE2 Application Settings
		//
		// Control the frame rate for an animation application
		this.maxFPS = 2.0;
		// Not adding controls but making the default buttons available
		this.controls.finishedAddingControls();
		this.enableControls = true;

		// wrap function and execute
		(() => {

			// this.resrcPath is the url of this folder
			d3.csv(`${this.resrcPath}data/mock.csv`,

				// parse data
				(d) => {

					// parse unix micro second timestamp to date object;
					const parsedTime = new Date(d.time / 1000000);
					d.time = parsedTime;

					return d;
				},

				// now use the data
				(data) => {
					const margin = {
						top: 50,
						right: 50,
						bottom: 50,
						left: 50
					};

					const width = this.sage2_width - margin.left - margin.right;
					const height = this.sage2_height - margin.top - margin.bottom;

					const svgContainer = d3.select(`#${this.element.id}`).append('svg')
						.attr("width", width + margin.left + margin.right)
						.attr("height", height + margin.top + margin.bottom)
						.append("g")
						.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

					const x = d3.scaleTime()
						.domain(d3.extent(data, (d) => {
							return d.time;
						}))
						.range([0, width]);
					svgContainer.append("g")
						.attr("transform", "translate(0," + height + ")")
						.call(d3.axisBottom(x));

					const y = d3.scaleLinear()
						.domain([0, d3.max(data, (d) => {
							return +d.mean_usage_system;
						})])
						.range([height, 0]);
					svgContainer.append("g")
						.call(d3.axisLeft(y));

					// Add the area
					svgContainer.append("path")
						.datum(data)
						.attr("fill", "#cce5df")
						.attr("stroke", "#69b3a2")
						.attr("stroke-width", 1.5)
						.attr("d", d3.area()
							.x((d) => {
								return x(d.time);
							})
							.y0(y(0))
							.y1((d) => {
								return y(d.mean_usage_system);
							})
						);
				});

		})();
	},

	load: function(date) {
		console.log('monitoring_dashboard> Load with state value', this.state.value);
		this.refresh(date);
	},

	draw: function(date) {
		console.log('monitoring_dashboard> Draw with state value', this.state.value);
	},

	resize: function(date) {
		// Called when window is resized
		this.refresh(date);
	},

	move: function(date) {
		// Called when window is moved (set moveEvents to continuous)
		this.refresh(date);
	},

	quit: function() {
		// Make sure to delete stuff (timers, ...)
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			// click
		} else if (eventType === "pointerMove" && this.dragging) {
			// move
		} else if (eventType === "pointerRelease" && (data.button === "left")) {
			// click release
		} else if (eventType === "pointerScroll") {
			// Scroll events for zoom
		} else if (eventType === "widgetEvent") {
			// widget events
		} else if (eventType === "keyboard") {
			if (data.character === "m") {
				this.refresh(date);
			}
		} else if (eventType === "specialKey") {
			if (data.code === 37 && data.state === "down") {
				// left
				this.refresh(date);
			} else if (data.code === 38 && data.state === "down") {
				// up
				this.refresh(date);
			} else if (data.code === 39 && data.state === "down") {
				// right
				this.refresh(date);
			} else if (data.code === 40 && data.state === "down") {
				// down
				this.refresh(date);
			}
		}
	}
});
