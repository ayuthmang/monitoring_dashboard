# monitoring_dashboard

A simple monitoring dashboard for sage2.

## How to install

Just copy this folder and place in `sage2/public/uploads/apps` and run sage2.

## Todo list

1. [ ] Using real data that comes from influxdb
